##Design Document

- what parts of human psychology do I want to capture in the operating system?
	- stress levels
    - happiness
    - learning
    - feeling-driven actions (clap, hit, throw, jump for joy etc)
    - maintenance tasks (workout, eat, drink, poop etc)
    - need for social engagement 
    - reproduction
    - wants and desires
    
- how will the os be implemented? what kind of an os do we need (embedded, general purpose)? Is this even a problem
that you need to write an os from scratch? The human component could just be a process on top of an embedded
OS. 

